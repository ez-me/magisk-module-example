# Magisk Module Example

This is in order to have a quick and easy reference. It's not a real module,
 please clone instead of fork.

## Long Description

I wanted to have an easy reference, instead of modifying another module that
 uses roughly the same features, so I just made a git repository to make it
 way easier for me and others.
Please clone instead of fork, it's not meant to be edited.

