# This file is sourced, not executed

# List all directories you want to directly replace in the system
REPLACE="
/system/app/test
"

print_modname() {
ui_print "*************************"
ui_print "  Magisk Module Example  "
ui_print "*************************"

# Silly examples of the variables we have
ui_print "Your Magisk version is: " $MAGISK_VER
ui_print "That's the code: "$MAGISK_VER_CODE
ui_print "Are we using the App? "$BOOTMODE
ui_print "Module path: " $MODPATH
ui_print "Temporary folder: " $TMPDIR
ui_print "Name of our Zip file: " $ZIPFILE
ui_print "Which arquitecture? " $ARCH
ui_print "Is that 64 bits? " $IS64BIT
ui_print "Which android version are we in? " $API

}

on_install() {
  ui_print "- Extracting module files"
  unzip -o "$ZIPFILE" 'system/*' -d $MODPATH >&2
}

set_permissions() {
  # The following is the default rule, DO NOT remove
  # user:group is 0:0
  # folders are rwxr-xr-x
  # files are rw-r--r--
  set_perm_recursive $MODPATH 0 0 0755 0644
}
