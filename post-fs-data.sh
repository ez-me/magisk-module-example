# This is executed in post-fs-data
# [post-fs-data] -> Zygisk -> Modules -> late_start 

# Example for adding more volume steps
# Use resetprop, NOT setprop
resetprop -n ro.config.vc_call_vol_steps 12
resetprop -n ro.config.media_vol_steps 24
